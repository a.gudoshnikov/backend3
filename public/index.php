<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_GET['save'])) {
      print('Спасибо, результаты сохранены.');
    }
    include('form.php');
    exit();
}

$result;

try{

    $errors = FALSE;
    if (empty($_POST['fio'])) {
        print('Заполните имя.<br/>');
        $errors = TRUE;
    }else if (!preg_match("/^[а-яА-Яa-zA-Z ]+$/u", $_POST['fio'])) {
    print('Недопустимые символы в имени.<br>');
    $errors = TRUE;
}

    if (empty($_POST['email'])) {
        print('Заполните почту.<br/>');
        $errors = TRUE;
    }

    if (empty($_POST['biography'])) {
        print('Заполните биографию.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['check'])) {
        print('Вы должны быть согласны с условиями.<br/>');
        $errors = TRUE;
    }

    if ($errors) {
        exit();
    }

    $name = $_POST['fio'];
    $email = $_POST['email'];
    $dob = $_POST['date'];
    $sex = $_POST['sex'];
    $limbs = $_POST['limbs'];
    $bio = $_POST['biography'];
    $che = $_POST['check'];
    
    $sup= implode(",",$_POST['superpower']);

    $conn = new PDO("mysql:host=localhost;dbname=u41048", 'u41048', '1308338', array(PDO::ATTR_PERSISTENT => true));

    
    $user = $conn->prepare("INSERT INTO form SET fio = ?, email = ?, your_date = ?, sex = ?, limbs = ?, bio = ?, accept = ?");
    $user -> execute([$_POST['fio'], $_POST['email'], date('Y-m-d', strtotime($_POST['date'])), $_POST['sex'], $_POST['limbs'], $_POST['biography'], $_POST['check']]);
    $id_user = $conn->lastInsertId();

    $user1 = $conn->prepare("INSERT INTO superpowers SET id = ?, your_power = ?");
    $user1 -> execute([$id_user, $sup]);
    $result = true;
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}

header('Location: ?save=1');

?>
