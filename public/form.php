<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&amp;display=swap"
        rel="stylesheet">
    <title>Задание 3</title>
</head>

<body>
    <form method="POST" id="form" action="index.php">
        <div class="form-inner">
            <h3>Расскажите о себе</h3>
            <label>Имя:<br />
                <input type=text name="fio" placeholder="Введите ваше имя" />
            </label><br />

            <label>E-mail:<br />
                <input name="email" placeholder="Введите ваш e-mail" type="email">
            </label><br />

            <label>Год рождения:<br />
                <input type="date" name="date">
            </label><br />

            <label>Пол:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="sex" value=1 />Мужской
            </label>
            <label class="radio"><input type="radio" name="sex" value=0 />Женский
            </label><br />

            <label>Выберите кол-во конечностей:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="limbs" value=0 />0
            </label>
            <label class="radio"><input type="radio" name="limbs" value=1 />1
            </label>
            <label class="radio"><input type="radio" name="limbs" value=2 />2
            </label>
            <label class="radio"><input type="radio" name="limbs" value=3 />3
            </label>
            <label class="radio"><input type="radio" name="limbs" value=4 />4
            </label><br />

            <label>Ваши сверхспособности:<br />
                <select multiple="true" name="superpower[]">
                    <option value="Бессмертие">Бессмертие</option>
                    <option value="Прохождение сквозь стены">Прохождение сквозь стены</option>
                    <option value="Левитация">Левитация</option>
                </select>
            </label><br />

            <label>
                Биография:<br />
                <textarea name="biography" placeholder="Расскажите о себе"></textarea>
                <br />
            </label>

            <label>
                <input name="check" type="checkbox" checked=checked value=1>С контрактом ознакомлен:<br />
            </label>

            <input type="submit" value="Отправить" />
        </div>
    </form>

    </div>

</body>

</html>
